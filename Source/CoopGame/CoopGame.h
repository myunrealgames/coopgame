// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#define DefaultFleshSurface SurfaceType1
#define VurenableFleshSurface SurfaceType2
#define WeaponCollision ECC_GameTraceChannel1
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuWidget.h"
#include "MainMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class COOPGAME_API UMainMenuWidget : public UMenuWidget
{
	GENERATED_BODY()

protected:
	virtual bool Initialize() override;
private:
	UPROPERTY(meta=(BindWidget))
	class UButton* HostButton;
	UPROPERTY(meta = (BindWidget))
	class UButton* JoinButton;
	UPROPERTY(meta = (BindWidget))
	class UWidgetSwitcher* MenuSwitcher;
	UPROPERTY(meta = (BindWidget))
	class UButton* JoinGameButton;
	UPROPERTY(meta = (BindWidget))
	class UEditableTextBox* IPTextBox;
	UPROPERTY(meta = (BindWidget))
	class UButton*  CancelButton;
	UPROPERTY(meta = (BindWidget))
	class UButton* ExitGameButton;
	UPROPERTY(meta = (BindWidget))
	class UEditableTextBox* PlayerNameTextBox;
	UFUNCTION()
	void HostServer();
	UFUNCTION()
	void OpenJoinMenu();
	UFUNCTION()
	void JoinGame();
	UFUNCTION()
	void CancleJoinMenu();
	UFUNCTION()
	void QuitGame();
};

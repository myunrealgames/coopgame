// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyMenuWidget.h"
#include "Components/WidgetSwitcher.h"
#include "Components/TextBlock.h"
#include "Engine/Classes/GameFramework/GameStateBase.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Engine\\Classes\\Kismet\\GameplayStatics.h"
#include "Net/UnrealNetwork.h"

bool ULobbyMenuWidget::Initialize()
{
	
	if (!Super::Initialize()) return false;
	if (!ensure(ClientServerSwitcher != nullptr)) return false;
	if (!ensure(PlayersText != nullptr)) return false;
	if (!ensure(NumOfPlayersText != nullptr)) return false;

	auto* world = GetWorld();
	if (!ensure(world != nullptr)) return false;
	APlayerController* playerController = world->GetFirstPlayerController();
	if (!ensure(playerController != nullptr)) return false;
	if (playerController->GetLocalRole() == ROLE_Authority)
	{
		ClientServerSwitcher->SetActiveWidgetIndex(1);
		FTimerHandle PlayerTimer;
		GetWorld()->GetTimerManager().SetTimer(PlayerTimer, this, &ULobbyMenuWidget::UpdatePlayers, 1, true, 0.0);
	}
	else
	{
		NumOfPlayersText->SetVisibility(ESlateVisibility::Hidden);
		ClientServerSwitcher->SetActiveWidgetIndex(0);
		PlayersText->SetVisibility(ESlateVisibility::Hidden);
	}

	
	return true;
}

void ULobbyMenuWidget::UpdatePlayers()
{

	int32 numberOfPlayers = GetWorld()->GetAuthGameMode()->GetNumPlayers();
	NumOfPlayersText->SetText(FText::FromString(FString::FromInt(numberOfPlayers)));
}


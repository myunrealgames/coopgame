// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuWidget.h"
#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Components/EditableTextBox.h"


bool UMainMenuWidget::Initialize()
{
	if (!Super::Initialize()) return false;
	if (!ensure(HostButton != nullptr)) return false;
	if (!ensure(JoinButton != nullptr)) return false;
	if (!ensure(JoinGameButton != nullptr)) return false;
	if (!ensure(IPTextBox != nullptr)) return false;
	if (!ensure(CancelButton != nullptr)) return false;
	if (!ensure(ExitGameButton != nullptr)) return false;
	if (!ensure(PlayerNameTextBox != nullptr)) return false;
	HostButton->OnClicked.AddDynamic(this, &UMainMenuWidget::HostServer);
	JoinButton->OnClicked.AddDynamic(this, &UMainMenuWidget::OpenJoinMenu);
	JoinGameButton->OnClicked.AddDynamic(this, &UMainMenuWidget::JoinGame);
	CancelButton->OnClicked.AddDynamic(this, &UMainMenuWidget::CancleJoinMenu);
	ExitGameButton->OnClicked.AddDynamic(this, &UMainMenuWidget::QuitGame);

	return true;
}



void UMainMenuWidget::HostServer()
{
	if (menuInterface != nullptr && !PlayerNameTextBox->GetText().IsEmpty())
	{
		menuInterface->SetPlayerName(PlayerNameTextBox->GetText().ToString());
		menuInterface->Host();
	}
}

void UMainMenuWidget::OpenJoinMenu()
{
	if (!PlayerNameTextBox->GetText().IsEmpty())
	{
		MenuSwitcher->SetActiveWidgetIndex(1);
	}
}

void UMainMenuWidget::JoinGame()
{
	if (menuInterface != nullptr && !IPTextBox->GetText().IsEmpty())
	{
		menuInterface->SetPlayerName(PlayerNameTextBox->GetText().ToString());
		menuInterface->Join(IPTextBox->GetText().ToString());
	}
}

void UMainMenuWidget::CancleJoinMenu()
{
	MenuSwitcher->SetActiveWidgetIndex(0);
}

void UMainMenuWidget::QuitGame()
{
	auto* world = GetWorld();
	if (!ensure(world != nullptr)) return;
	APlayerController* playerController = world->GetFirstPlayerController();
	if (!ensure(playerController != nullptr)) return;
	playerController->ConsoleCommand("quit");
}

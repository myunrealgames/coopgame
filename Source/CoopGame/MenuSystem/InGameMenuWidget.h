// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuWidget.h"
#include "InGameMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class COOPGAME_API UInGameMenuWidget : public UMenuWidget
{
	GENERATED_BODY()

protected:
	virtual bool Initialize() override;
private:
	UPROPERTY(meta = (BindWidget))
	class UButton* BackButton;
	UPROPERTY(meta = (BindWidget))
	class UButton* ExitButton;
	UFUNCTION()
	void ResumeGame();
	UFUNCTION()
	void ExitGame();
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuWidget.h"

void UMenuWidget::SetMenuInterface(IMenuInterface* iface)
{
	menuInterface = iface;
}

void UMenuWidget::Setup(bool bIsUIInput)
{
	AddToViewport();
	auto* world = GetWorld();
	if (!ensure(world != nullptr)) return;

	APlayerController* playerController = world->GetFirstPlayerController();
	if (!ensure(playerController != nullptr)) return;
	if (bIsUIInput)
	{
		FInputModeUIOnly UIInputMode;
		UIInputMode.SetWidgetToFocus(TakeWidget());
		UIInputMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
		playerController->SetInputMode(UIInputMode);
		playerController->bShowMouseCursor = true;
	}
	else
	{
		FInputModeGameOnly GameInputMode;
		GameInputMode.SetConsumeCaptureMouseDown(true);
		playerController->SetInputMode(GameInputMode);
		playerController->bShowMouseCursor = false;
	}
	
}

void UMenuWidget::TearDown()
{
	
	auto* world = GetWorld();
	if (!ensure(world != nullptr)) return;
	APlayerController* playerController = world->GetFirstPlayerController();
	if (!ensure(playerController != nullptr)) return;
	FInputModeGameOnly GameInputMode;
	GameInputMode.SetConsumeCaptureMouseDown(true);
	playerController->SetInputMode(GameInputMode);
	playerController->bShowMouseCursor = false;
	RemoveFromViewport();
}

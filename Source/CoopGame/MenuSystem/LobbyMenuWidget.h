// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuWidget.h"
#include "LobbyMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class COOPGAME_API ULobbyMenuWidget : public UMenuWidget
{
	GENERATED_BODY()
protected:
	virtual bool Initialize() override;
	void UpdatePlayers();
private:
	UPROPERTY(meta = (BindWidget))
	class UWidgetSwitcher* ClientServerSwitcher;
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* PlayersText;
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* NumOfPlayersText;
public:
	void UpdatePlayerText(int32 numberOfPlayers);
	
	
};

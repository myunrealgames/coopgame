// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameMenuWidget.h"
#include "Components/Button.h"

bool UInGameMenuWidget::Initialize()
{
	if (!Super::Initialize()) return false;
	if (!ensure(BackButton != nullptr)) return false;
	if (!ensure(ExitButton != nullptr)) return false;
	BackButton->OnClicked.AddDynamic(this, &UInGameMenuWidget::ResumeGame);
	ExitButton->OnClicked.AddDynamic(this, &UInGameMenuWidget::ExitGame);
	return true;
}

void UInGameMenuWidget::ResumeGame()
{
	TearDown();
}

void UInGameMenuWidget::ExitGame()
{
	if (menuInterface != nullptr)
		menuInterface->GoToMainMenu();
}

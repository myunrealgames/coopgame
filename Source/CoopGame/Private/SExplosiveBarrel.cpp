// Fill out your copyright notice in the Description page of Project Settings.


#include "SExplosiveBarrel.h"
#include "Components/SHealth.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ASExplosiveBarrel::ASExplosiveBarrel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetSimulatePhysics(true);
	MeshComp->SetCollisionObjectType(ECC_PhysicsBody);
	RootComponent = MeshComp;
	HealthComp = CreateDefaultSubobject<USHealth>(TEXT("HealthComp"));
	HealthComp->OnHealthChanged.AddDynamic(this, &ASExplosiveBarrel::OnHealthChanged);
	RadialForceComp = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForceComp"));
	RadialForceComp->SetupAttachment(MeshComp);
	
	RadialForceComp->bImpulseVelChange = true;
	RadialForceComp->bAutoActivate = false;
	RadialForceComp->bIgnoreOwningActor = true;
	SetReplicates(true);
	SetReplicateMovement(true);
}

// Called when the game starts or when spawned
void ASExplosiveBarrel::BeginPlay()
{
	Super::BeginPlay();
	RadialForceComp->Radius = DamageOuterRadius;
}

void ASExplosiveBarrel::OnHealthChanged(USHealth* HealthCom, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if(bExploded) return;
	if (Health <= 0.0 && ExplosionEffect)
	{
		bExploded = true;
		On_bExplodedRep();
		FVector actorOrigin, ActorBounds;
		GetActorBounds(true, actorOrigin, ActorBounds);
		FVector height = FVector(0.0, 0.0, ActorBounds.Z);
		FVector HitDirection = FVector::ZeroVector;
		if (DamageCauser)
		{
			FVector CauserLocation = DamageCauser->GetActorLocation();
			HitDirection = actorOrigin - CauserLocation;
			HitDirection.Z = 0;
			HitDirection.Normalize();
			HitDirection *= 100;
		}
		HitDirection.Z = 500;
		MeshComp->AddImpulse(HitDirection * MeshComp->GetMass());
		
		//UGameplayStatics::ApplyRadialDamageWithFalloff(this,BaseDamage, MinimumDamage, GetActorLocation(), 
 	//	DamageInnerRadius, DamageOuterRadius, 1, DamageTypeClass, ignore, DamageCauser, InstigatedBy);
		RadialForceComp->FireImpulse();
		TArray<AActor*> ignore;
		ignore.Add(this);
		UGameplayStatics::ApplyRadialDamage(this, BaseDamage, GetActorLocation(), DamageInnerRadius, DamageTypeClass, ignore,
			this, InstigatedBy, true);
		if (ExplosionSoundEffect)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplosionSoundEffect, GetActorLocation());
		}
		if (ExplodedMaterial)
		{
			MeshComp->SetMaterial(0, ExplodedMaterial);
		}
	}
}

void ASExplosiveBarrel::On_bExplodedRep()
{
	FVector actorOrigin, ActorBounds;
	GetActorBounds(true, actorOrigin, ActorBounds);
	FVector height = FVector(0.0, 0.0, ActorBounds.Z);
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorLocation() + height, GetActorRotation());

}

// Called every frame
void ASExplosiveBarrel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASExplosiveBarrel::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ASExplosiveBarrel, bExploded);
}
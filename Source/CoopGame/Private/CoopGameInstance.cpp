// Fill out your copyright notice in the Description page of Project Settings.


#include "CoopGameInstance.h"
#include "BluePrint/UserWidget.h"
#include "UObject/ConstructorHelpers.h"
#include "CoopGame/MenuSystem/MainMenuWidget.h"
#include "CoopGame/MenuSystem/InGameMenuWidget.h"
#include "CoopGame/MenuSystem/LobbyMenuWidget.h"
#include "SCharacter.h"

UCoopGameInstance::UCoopGameInstance()
{
	ConstructorHelpers::FClassFinder<UUserWidget> MainMenuBPClass(TEXT("/Game/MenuSystem/WBP_MenuSystem"));
	if (!ensure(MainMenuBPClass.Class != nullptr)) return;
	MainMenuClass = MainMenuBPClass.Class;

	ConstructorHelpers::FClassFinder<UUserWidget> InGameMenuBPClass(TEXT("/Game/MenuSystem/WBP_InGameMenu"));
	if (!ensure(InGameMenuBPClass.Class != nullptr)) return;
	InGameMenuClass = InGameMenuBPClass.Class;

	ConstructorHelpers::FClassFinder<UUserWidget> LobbyMenuBPClass(TEXT("/Game/MenuSystem/WBP_LobbyMenu"));
	if (!ensure(LobbyMenuBPClass.Class != nullptr)) return;
	LobbyMenuClass = LobbyMenuBPClass.Class;
}

void UCoopGameInstance::Host()
{
	if (!ensure(mainMenu != nullptr)) return;
	mainMenu->TearDown();
	UWorld* world = GetWorld();
	if (!ensure(world != nullptr)) return;
	world->ServerTravel("Lobby?listen");
}


void UCoopGameInstance::Join(FString ipAddress)
{
	serverIp = ipAddress;
	if (!ensure(mainMenu != nullptr)) return;
	mainMenu->TearDown();
	APlayerController* controller = GetFirstLocalPlayerController();
	if (!ensure(controller != nullptr)) return;
	controller->ClientTravel(ipAddress, ETravelType::TRAVEL_Absolute);
	
}

void UCoopGameInstance::StartLevel()
{
	if (!ensure(lobbyMenu != nullptr)) return;
	lobbyMenu->TearDown();
	UWorld* world = GetWorld();
	if (!ensure(world != nullptr)) return;
	APlayerController* controller = GetFirstLocalPlayerController();
	if (!ensure(controller != nullptr)) return;
	if (controller->GetLocalRole() == ROLE_Authority)
	{
		world->ServerTravel("Blockout_P?listen");
	}
}

void UCoopGameInstance::GoToMainMenu()
{
	if(!ensure(inGameMenu != nullptr)) return;
	inGameMenu->TearDown();
	APlayerController* controller = GetFirstLocalPlayerController();
	if (!ensure(controller != nullptr)) return;
	controller->ClientTravel("/Game/MenuSystem/MenuLevel", ETravelType::TRAVEL_Absolute);
}

void UCoopGameInstance::SetPlayerName(FString newName)
{
	name = newName;
	/*ASCharacter* Character = Cast<ASCharacter>(GetFirstLocalPlayerController()->GetCharacter());
	Character->SetName(newName);*/
}

void UCoopGameInstance::ShowMainMenu()
{
	if (!ensure(MainMenuClass != nullptr)) return;
	 mainMenu = CreateWidget<UMainMenuWidget>(this, MainMenuClass);
	if (!ensure(mainMenu != nullptr)) return;
	
	mainMenu->SetMenuInterface(this);
	mainMenu->Setup(true);
}

void UCoopGameInstance::ShowInGameMenu()
{
	if (!ensure(InGameMenuClass != nullptr)) return;
	inGameMenu = CreateWidget<UInGameMenuWidget>(this, InGameMenuClass);
	if (!ensure(inGameMenu != nullptr)) return;

	inGameMenu->SetMenuInterface(this);
	inGameMenu->Setup(true);
}

void UCoopGameInstance::ShowLobbyMenu()
{
	if (!ensure(LobbyMenuClass != nullptr)) return;
	lobbyMenu = CreateWidget<ULobbyMenuWidget>(this, LobbyMenuClass);
	if (!ensure(lobbyMenu != nullptr)) return;

	lobbyMenu->SetMenuInterface(this);
	lobbyMenu->Setup(false);
}


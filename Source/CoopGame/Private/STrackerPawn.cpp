// Fill out your copyright notice in the Description page of Project Settings.


#include "STrackerPawn.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"
#include "Runtime/NavigationSystem/Public/NavigationSystem.h"
#include "Runtime/NavigationSystem/Public/NavigationPath.h"
#include "Components/SHealth.h"
#include "Runtime/Core/Public/CoreMinimal.h"
#include "Runtime/Engine/Classes/Sound/SoundCue.h"
#include "Components/AudioComponent.h"
// Sets default values
ASTrackerPawn::ASTrackerPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	meshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	meshComp->SetCanEverAffectNavigation(false);
	meshComp->SetSimulatePhysics(true);
	RootComponent = meshComp;
	minDistanceToNextPoint = 100;
	forcePower = 1000;
	damage = 100;
	explosionRadius = 300;
	HealthComp = CreateDefaultSubobject<USHealth>(TEXT("HealthComp"));
	HealthComp->OnHealthChanged.AddDynamic(this, &ASTrackerPawn::OnHealthChanged);
	sphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SPhereComp"));
	sphereComp->SetupAttachment(RootComponent);
	sphereComp->SetSphereRadius(explosionRadius);
	sphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	sphereComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	sphereComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	sphereComp->SetCollisionResponseToChannel(ECC_EngineTraceChannel1, ECR_Ignore);
	audioComp = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComp"));
	audioComp->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ASTrackerPawn::BeginPlay()
{
	Super::BeginPlay();
	GetNextPathPoint();

}

void ASTrackerPawn::GetNextPathPoint()
{
	if (GetLocalRole() == ROLE_Authority)
	{
		ACharacter* achar = GetClosestCharacter();
		if (achar == nullptr) return;
		UNavigationPath* pathToNext = UNavigationSystemV1::FindPathToActorSynchronously(GetWorld(), GetActorLocation(), achar);
		if (pathToNext != nullptr && pathToNext->PathPoints.Num() > 1)
		{
			nextPoint = pathToNext->PathPoints[1];
		}
		else
		{
			nextPoint = achar->GetActorLocation();
		}
	}
}

void ASTrackerPawn::OnHealthChanged(USHealth* HealthCom, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	//UE_LOG(LogTemp, Warning, TEXT("Spike Got Hit! %f"), Health);
	if (MatInst == nullptr)
	{
		MatInst = meshComp->CreateAndSetMaterialInstanceDynamicFromMaterial(0, meshComp->GetMaterial(0));
	}
	if (MatInst)
	{
		MatInst->SetScalarParameterValue("LastTimeDamageTaken", GetWorld()->TimeSeconds);
	}
	if (Health <= 0)
	{
		Explode();
	}
}

void ASTrackerPawn::Explode()
{
	if (exploded) return;
	exploded = true;
	TArray<AActor*> ignore;
	ignore.Add(this);
	meshComp->SetVisibility(false);
	meshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	if (ExplosionEffect)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorLocation(), GetActorRotation());
		if (ExplosionSoundEffect)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplosionSoundEffect, GetActorLocation());
		}
	}
	if (GetLocalRole() == ROLE_Authority)
	{
		UGameplayStatics::ApplyRadialDamage(this, damage, GetActorLocation(), explosionRadius, nullptr, ignore, this, GetInstigatorController(), true);
		SetLifeSpan(2.0);
	}
}

// Called every frame
void ASTrackerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (GetLocalRole() == ROLE_Authority)
	{

		float distance = (nextPoint - GetActorLocation()).Size();
		if (distance < minDistanceToNextPoint)
		{

			UE_LOG(LogTemp, Warning, TEXT("Spike Too Close"));

			GetNextPathPoint();
		}
		else
		{
			FVector forceDir = (nextPoint - GetActorLocation());
			forceDir.Normalize();
			forceDir *= forcePower;
			meshComp->AddForce(forceDir, NAME_None, true);
			UE_LOG(LogTemp, Warning, TEXT("Spike Vel =  %f"), GetVelocity().Size());

		}
		if (GetVelocity().Size() < 10)
		{
			GetNextPathPoint();
			meshComp->AddForce(FMath::VRand() * 3 * forcePower, NAME_None, true);
		}
	}
}

void ASTrackerPawn::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	ACharacter* checkIfChar = Cast<ACharacter>(OtherActor);
	if (checkIfChar && !USHealth::IsFriendly(this, checkIfChar))
	{
		UGameplayStatics::ApplyDamage(this, 100, GetInstigatorController(), this, nullptr);
	}
}

ACharacter* ASTrackerPawn::GetClosestCharacter()
{
	float closestDist = FLT_MAX;
	ACharacter* closestActor = nullptr;
	for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; it++)
	{
		APlayerController* PC = it->Get();
		if (PC)
		{
			ACharacter* character = PC->GetCharacter();
			if (character)
			{
				if (!USHealth::IsFriendly(this, character))
				{
					FVector actorLocation = character->GetActorLocation();
					float distToActor = FVector::Distance(GetActorLocation(), actorLocation);
					if (distToActor <= closestDist)
					{
						closestDist = distToActor;
						closestActor = character;
					}
				}
			}
		}
	}
	return closestActor;
}


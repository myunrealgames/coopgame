// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/SHealth.h"
#include "Net/UnrealNetwork.h"
#include <Runtime/Engine/Classes/Components/ActorComponent.h>
#include <Runtime/Engine/Classes/GameFramework/PainCausingVolume.h>

// Sets default values for this component's properties
USHealth::USHealth()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.

	// ...
	
}


// Called when the game starts
void USHealth::BeginPlay()
{
	Super::BeginPlay();
	SetIsReplicated(true);
	if(GetOwnerRole() == ROLE_Authority)
	{
		AActor* owner = GetOwner();
		if (owner)
		{
			owner->OnTakeAnyDamage.AddDynamic(this, &USHealth::OnTakeAnyDamageHandler);
		}
	}
	Health = DefaultHealth;
	// ...
	
}

void USHealth::Rep_OnHealth(float oldHealth)
{
	float Damage = oldHealth - Health;
	OnHealthChanged.Broadcast(this, Health, Damage, nullptr, nullptr, nullptr);
}

void USHealth::OnTakeAnyDamageHandler(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if(Damage <= 0)
		return;
	if (IsFriendly(DamagedActor, DamageCauser))
	{
		return;
	}
	if (bIsActiveShield)
	{
		Damage /= 2.0;
	}
	Health = FMath::Clamp(Health-Damage, 0.0f, DefaultHealth);
	OnHealthChanged.Broadcast(this,Health, Damage, DamageType, InstigatedBy, DamageCauser);

}

float USHealth::GetHealth() const
{
	return Health;
}

void USHealth::Heal(float healAmount)
{
	if (healAmount < 0.0f) return;
	Health = FMath::Clamp(Health + healAmount, 0.0f, DefaultHealth);
	OnHealthChanged.Broadcast(this, Health, -healAmount, nullptr, nullptr, nullptr);
}

bool USHealth::IsFriendly(AActor* Actor1, AActor* Actor2)
{
	if (Actor1 == nullptr || Actor2 == nullptr)
	{
		return true;
	}
	if (Actor1 == Actor2 )
	{
		return false;
	}
	if (Actor1->GetClass()->IsChildOf(APainCausingVolume::StaticClass()) 
		|| Actor2->GetClass()->IsChildOf(APainCausingVolume::StaticClass()))
	{
		return false;
	}
	USHealth* Health1 = Cast<USHealth>(Actor1->GetComponentByClass(USHealth::StaticClass()));
	USHealth* Health2 = Cast<USHealth>(Actor2->GetComponentByClass(USHealth::StaticClass()));
	if (Health1 == nullptr)
	{
		return true;
	}
	if (Health2 == nullptr)
	{
		return false;
	}
	return Health1->TeamId == Health2->TeamId;
}

uint8 USHealth::GetTeamId() const
{
	return TeamId;
}

void USHealth::SetShield(bool activate)
{
	bIsActiveShield = activate;
}

void USHealth::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(USHealth, Health);
}


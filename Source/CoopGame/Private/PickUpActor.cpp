// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUpActor.h"
#include "Components/SphereComponent.h"
#include "Components//DecalComponent.h"
#include "PowerUpActor.h"
#include "Runtime\Engine\Public\TimerManager.h"
#include "SCharacter.h"
// Sets default values
APickUpActor::APickUpActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	sphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SPhereComp"));
	RootComponent = sphereComp;
	sphereComp->SetSphereRadius(75.0f);
	decalComp = CreateDefaultSubobject<UDecalComponent>(TEXT("Decal"));
	decalComp->DecalSize = FVector(64, 75, 75);
	decalComp->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f));
	decalComp->SetupAttachment(RootComponent);
	respawnRate = 10.0;
	currentPowerUp = nullptr;

}

// Called when the game starts or when spawned
void APickUpActor::BeginPlay()
{
	Super::BeginPlay();
	SetReplicates(true);
	Respawm();
	
}

void APickUpActor::Respawm()
{
	if (GetLocalRole() == ROLE_Authority)
	{
		FActorSpawnParameters spawnParams;
		spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		currentPowerUp = GetWorld()->SpawnActor<APowerUpActor>(powerupClass, GetTransform(), spawnParams);
	}
}

// Called every frame
void APickUpActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickUpActor::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);
	if (!OtherActor->GetClass()->IsChildOf(ASCharacter::StaticClass())) return;
	if (GetLocalRole() != ROLE_Authority || currentPowerUp == nullptr) return;
	currentPowerUp->ActivatePowerUp(OtherActor);
	currentPowerUp = nullptr;
	if (respawnRate > 0.0)
	{
		FTimerHandle timer;
		GetWorldTimerManager().SetTimer(timer, this, &APickUpActor::Respawm, respawnRate, false, respawnRate);
	}

}


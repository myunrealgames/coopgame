// Fill out your copyright notice in the Description page of Project Settings.

#include "SCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include <Runtime\Engine\Classes\Engine\World.h>
#include "CoopGame/CoopGame.h"
#include "Components/CapsuleComponent.h"
#include "Components/SHealth.h"
#include "Net/UnrealNetwork.h"
#include "CoopGameInstance.h"
#include "GameFramework/PlayerState.h"
// Sets default values	
ASCharacter::ASCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComp"));
	SpringArmComp->SetupAttachment(RootComponent);
	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	CameraComp->SetupAttachment(SpringArmComp);
	HealthComp = CreateDefaultSubobject<USHealth>(TEXT("HealthComp"));
	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true; 
	GetMesh()->SetCollisionResponseToChannel(ECC_Visibility,ECR_Block);
	GetCapsuleComponent()->SetCollisionResponseToChannel(WeaponCollision,ECR_Ignore);
	HealthComp->OnHealthChanged.AddDynamic(this, &ASCharacter::OnHealthChanged);
}

// Called when the game starts or when spawned
void ASCharacter::BeginPlay()
{
	Super::BeginPlay();
	defaultFOV = CameraComp->FieldOfView;
	if (GetLocalRole() == ROLE_Authority)
	{
		FActorSpawnParameters spawnParameters;
		spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		if (FMath::RandRange(0, 100) < chanceForSecondary)
		{
			currentWeapon = GetWorld()->SpawnActor<ASWeapon>(secondaryStartWeaponClass, FVector::ZeroVector, FRotator::ZeroRotator, spawnParameters);
		}
		else
		{
			currentWeapon = GetWorld()->SpawnActor<ASWeapon>(startWeaponClass, FVector::ZeroVector, FRotator::ZeroRotator, spawnParameters);
		}
		
		if (currentWeapon)
		{
			currentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, WeaponSocketName);
			currentWeapon->SetOwner(this);
		}
	}

	GetWorld()->GetTimerManager().SetTimer(NameTimer, this, &ASCharacter::SetName, 5.0, false, 5.0);
}

void ASCharacter::MoveForward(float value)
{
	AddMovementInput(GetActorForwardVector() * value);
}

void ASCharacter::MoveRight(float value)
{
	AddMovementInput(GetActorRightVector() * value);

}

void ASCharacter::BeginCrouch()
{
	Crouch();
}

void ASCharacter::EndCrouch()
{
	UnCrouch();
}

void ASCharacter::DoJump()
{
	Jump();
}

void ASCharacter::BeginZoom()
{
	bZoom = true;
}

void ASCharacter::EndZoom()
{
	bZoom = false;
}

void ASCharacter::StartFire()
{
	if (currentWeapon)
	{
		currentWeapon->StartFire();
	}
}

void ASCharacter::StopFire()
{
	if (currentWeapon)
	{
		currentWeapon->StopFire();
	}
}


void ASCharacter::SetName()
{
	
	if(!(Cast<UCoopGameInstance>(GetGameInstance())->name.IsEmpty()))
		SetName_BP(Cast<UCoopGameInstance>(GetGameInstance())->name);
}

void ASCharacter::OnHealthChanged(USHealth* HealthCom, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (Health <= 0.0 && !bDied)
	{
		bDied = true;
		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		DetachFromControllerPendingDestroy();
		SetLifeSpan(10.0f);
		ASCharacter::StopFire();
	}

}


bool ASCharacter::GetDied()
{
	return bDied;
}

// Called every frame
void ASCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float TargetFOV = bZoom ? zoomFOV : defaultFOV;
	float currentFOV = FMath::FInterpTo(CameraComp->FieldOfView, TargetFOV, DeltaTime, zoomVelocity);
	CameraComp->SetFieldOfView(currentFOV);
}

// Called to bind functionality to input
void ASCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &ASCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASCharacter::MoveRight);
	PlayerInputComponent->BindAxis("LookUp", this, &ASCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn", this, &ASCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAction("Crouch",IE_Pressed ,this, &ASCharacter::BeginCrouch);
	PlayerInputComponent->BindAction("Crouch",IE_Released ,this, &ASCharacter::EndCrouch);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ASCharacter::DoJump);
	PlayerInputComponent->BindAction("Zoom", IE_Pressed, this, &ASCharacter::BeginZoom);
	PlayerInputComponent->BindAction("Zoom", IE_Released, this, &ASCharacter::EndZoom);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ASCharacter::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ASCharacter::StopFire);
}


FVector ASCharacter::GetPawnViewLocation() const
{
	if (CameraComp)
	{
		return CameraComp->GetComponentLocation();
	}
	return Super::GetPawnViewLocation();
}

void ASCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ASCharacter, currentWeapon);
	DOREPLIFETIME(ASCharacter, bDied);
}

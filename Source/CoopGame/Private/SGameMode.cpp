// Fill out your copyright notice in the Description page of Project Settings.


#include "SGameMode.h"
#include "Components/SHealth.h"
#include "Engine\\Classes\\Kismet\\GameplayStatics.h"
#include "SCharacter.h"
#include "STrackerPawn.h"

ASGameMode::ASGameMode()
{
	PrimaryActorTick.bCanEverTick = true;
}
void ASGameMode::StartBotsWave()
{

	numberOfBotsToSpawn = IncreaseBotsEachSpwan ? (waveNumber+1) * SpawnIncreaseMultiply : 1;
	waveNumber++;
	GetWorldTimerManager().SetTimer(TimerHandle_BotSpawn, this, &ASGameMode::SpawnBotTimerElappsed, 1.0, true, 0.0);

}

void ASGameMode::EndBotWave()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_BotSpawn);
	PrepareForNextWave();	
}

void ASGameMode::PrepareForNextWave()
{
	if (waveNumber < WavesToComplete && PawnWaveTime > 0.0 && FirstPawnWaveTime > 0.0)
	{
		float WaveTime = waveNumber == 0 ? FirstPawnWaveTime : PawnWaveTime;
		GetWorldTimerManager().SetTimer(TimerHandle_StartWave, this, &ASGameMode::StartBotsWave, WaveTime, false);
	}
}

void ASGameMode::SpawnBotTimerElappsed()
{
	SpawmNewBot();
	numberOfBotsToSpawn--;
	if (numberOfBotsToSpawn <= 0)
	{
		EndBotWave();
	}

}

void ASGameMode::CheckWaveState()
{
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASTrackerPawn::StaticClass(), FoundActors);
	for (auto tracker : FoundActors)
	{
		USHealth* healtComopnent = Cast<USHealth>(tracker->GetComponentByClass(USHealth::StaticClass()));
		if (healtComopnent && healtComopnent->GetHealth() > 0) //atlease 1 alive enemy
		{
			return;
		}
	}
	FoundActors.Empty();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASCharacter::StaticClass(), FoundActors);
	for (auto aiCharacter : FoundActors)
	{
		USHealth* healtComopnent = Cast<USHealth>(aiCharacter->GetComponentByClass(USHealth::StaticClass()));
		if (healtComopnent && healtComopnent->GetTeamId() != 0 && healtComopnent->GetHealth() > 0) //atlease 1 alive enemy
		{
			return;
		}
	}
	if (waveNumber < WavesToComplete) //all wave's enemies were killed. time to spawn new ones...
	{
		if (numberOfBotsToSpawn == 0 && waveNumber > 0)
		{
			GetWorldTimerManager().ClearTimer(TimerHandle_StartWave);
			StartBotsWave();
		}
	}
	else
	{
		GameOver(true);
	}
}

bool ASGameMode::IsAnyPlayerAlive()
{
	for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; it++)
	{
		APlayerController* PC = it->Get();
		if (PC && PC->GetPawn())
		{
			APawn* pawn = PC->GetPawn();
			USHealth* healtComopnent = Cast<USHealth>(pawn->GetComponentByClass(USHealth::StaticClass()));
			if (ensure(healtComopnent))
			{
				if (healtComopnent->GetHealth() > 0.0)
				{
					return true;
				}
			}
		}
	}
	return false;

}

void ASGameMode::GameOver(bool bDidWinGame)
{
	gameOver = true;
	if (spectetingActorClass)
	{
		TArray<AActor*> FoundActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), spectetingActorClass, FoundActors);
		if (FoundActors.Num() > 0)
		{
			for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; it++)
			{
				APlayerController* PC = it->Get();
				if (PC)
				{
					PC->SetViewTargetWithBlend(FoundActors[0], 0.5f, VTBlend_Cubic);
				}
			}
		}
	}


}



void ASGameMode::PreLogin(const FString& Options, const FString& Address, const FUniqueNetIdRepl& UniqueId, FString& ErrorMessage)
{
	Super::PreLogin(Options, Address, UniqueId, ErrorMessage);
	if (waveNumber > 0)
	{
		ErrorMessage += "Sorry, Game Already Started";
	}
}

void ASGameMode::StartPlay()
{
	try
	{
		Super::StartPlay();
	}
	catch (...)
	{
	}

	PrepareForNextWave();
}

void ASGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!gameOver)
	{
		CheckWaveState();
		if (!IsAnyPlayerAlive())
		{
			GameOver(false);
		}
	}
}


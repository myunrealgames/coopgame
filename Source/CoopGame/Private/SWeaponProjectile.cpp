// Fill out your copyright notice in the Description page of Project Settings.


#include "SWeaponProjectile.h"
#include "Components/SkeletalMeshComponent.h"

// Sets default values
ASWeaponProjectile::ASWeaponProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASWeaponProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASWeaponProjectile::Fire()
{
	AActor* myOwner = GetOwner();
	if (myOwner&& ProjectileActor)
	{
		FHitResult hitResult;
		FVector ownerLocation;
		FRotator ownerRotation;
		myOwner->GetActorEyesViewPoint(ownerLocation, ownerRotation);
		FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);
		FActorSpawnParameters spawnParameters;
		spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		GetWorld()->SpawnActor<AActor>(ProjectileActor, MuzzleLocation, ownerRotation, spawnParameters);
	}

}

// Called every frame
void ASWeaponProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


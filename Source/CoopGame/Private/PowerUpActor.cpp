// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerUpActor.h"
#include "Runtime\Engine\Public\TimerManager.h"
#include "Net/UnrealNetwork.h"
// Sets default values
APowerUpActor::APowerUpActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	tickDuration = 0.0;
	numOfTicks = 0;
	tickPassed = 0;
	ended = false;
	SetActorEnableCollision(false);
	SetReplicates(true);
}

// Called when the game starts or when spawned
void APowerUpActor::BeginPlay()
{
	Super::BeginPlay();
	
}

void APowerUpActor::ActivatePowerUp(AActor* activator)
{
	if(tickDuration > 0.0)
	{ 
		BeginPowerUp(activator);
		isPowerUpActive = true;
		OnRep_PowerUpActive();
		GetWorldTimerManager().SetTimer(PowerUpTickTimer,this, &APowerUpActor::OnPowerUpTick,tickDuration,true, 0.0);
	}

}

void APowerUpActor::OnPowerUpTick()
{
	tickPassed++;
	if (tickPassed > numOfTicks && !ended)
	{
		ended = true;
		EndPowerUp();
		isPowerUpActive = false;
		OnRep_PowerUpActive();
		
		GetWorldTimerManager().ClearTimer(PowerUpTickTimer);
	}
	else
	{
		PowerupTicked();
	}
}

// Called every frame
void APowerUpActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APowerUpActor::OnRep_PowerUpActive()
{
	if (isPowerUpActive)
	{
		PowerUpActiveStateChanged(isPowerUpActive);
	}
	else
	{

	}
}

void APowerUpActor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APowerUpActor, isPowerUpActive);
}
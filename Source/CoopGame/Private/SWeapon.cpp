// Fill out your copyright notice in the Description page of Project Settings.


#include "SWeapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Components/SkeletalMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "HAL/ConsoleManager.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "CoopGame/CoopGame.h"
#include "Net/UnrealNetwork.h"

static int32 DebugWeaponDrawing = 0;
FAutoConsoleVariableRef CVarDebugWeaponDrawing(
	TEXT("COOP.DebugWeapons"),
	DebugWeaponDrawing,
	TEXT("Draw Debug Lines for Weapons"),
	ECVF_Cheat);
// Sets default values
ASWeapon::ASWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshComp"));
	RootComponent = MeshComp;
	MuzzleSocketName = "MuzzleSocket";
	TraceParameterName = "Target";
	SetReplicates(true);
	NetUpdateFrequency = 66.0;
	MinNetUpdateFrequency = 33.0;
	BulletSpeard = 2.0;
}
void ASWeapon::BeginPlay()
{
	DelayBetweenShotsSeconds = 60.0 / FireRate;
}

void ASWeapon::ServerFire_Implementation()
{
	Fire();
}

bool ASWeapon::ServerFire_Validate()
{
	return true;
}

void ASWeapon::On_HitScanTrace_Rep()
{
	PlayFireEffect(HitScanTrace.TraceTo);
	PlayHitEffect(HitScanTrace.PhysicalSurface, HitScanTrace.TraceTo);
}
void ASWeapon::Fire()
{
	if (GetLocalRole() < ROLE_Authority)
	{
		ServerFire();
	}
	AActor* myOwner = GetOwner();
	if (FireDurationBeforeCoolDown > 0 && !bIsInFireCooldown && !bIsCountingCooldown)
	{
		bIsCountingCooldown = true;
		GetWorld()->GetTimerManager().SetTimer(fireCooldownHandler, this, &ASWeapon::CoolDown, FireDurationBeforeCoolDown, false);
	}
	if (myOwner && !bIsInFireCooldown)
	{
		FHitResult hitResult;
		FVector ownerLocation;
		FRotator ownerRotation;
		FCollisionQueryParams collisionParams;
		collisionParams.AddIgnoredActor(myOwner);
		collisionParams.AddIgnoredActor(this);
		collisionParams.bTraceComplex = true;
		collisionParams.bReturnPhysicalMaterial = true;

		myOwner->GetActorEyesViewPoint(ownerLocation, ownerRotation);
		FVector shotDirection = ownerRotation.Vector();
		float BullerSpreadRad = FMath::DegreesToRadians(BulletSpeard);
		shotDirection = FMath::VRandCone(shotDirection, BullerSpreadRad, BullerSpreadRad);
		FVector finalPoint = ownerLocation + (shotDirection * 10000);
		FVector traceTarget = finalPoint;
		EPhysicalSurface surface = SurfaceType_Default;
		if (GetWorld()->LineTraceSingleByChannel(hitResult, ownerLocation, finalPoint, WeaponCollision, collisionParams))
		{
			//collision detected for the trace
			
			surface = hitResult.PhysMaterial.Get()->SurfaceType;
			PlayHitEffect(surface, hitResult.ImpactPoint);
			float acctualDamage = BaseDamage;
			if (surface == VurenableFleshSurface)
			{
				acctualDamage*= 4;
			}
			UGameplayStatics::ApplyPointDamage(hitResult.GetActor(), acctualDamage, ownerLocation, hitResult,
				myOwner->GetInstigatorController(), myOwner, DamageType);
			traceTarget = hitResult.Location;
		}
		PlayFireEffect(traceTarget);
		if(GetLocalRole() == ROLE_Authority)
		{
			HitScanTrace.PhysicalSurface = surface;
			HitScanTrace.TraceTo = traceTarget;
			HitScanTrace.changeForcer = (HitScanTrace.changeForcer+1)%2;
		}
		if(DebugWeaponDrawing)
			DrawDebugLine(GetWorld(), ownerLocation, finalPoint, FColor::Red, false, 1.0f, 0, 1.0f);
	}
	lastTimeFire = GetWorld()->TimeSeconds;
}



void ASWeapon::PlayHitEffect(EPhysicalSurface surface, FVector ImpactPoint)
{
	UParticleSystem* HitEffect = nullptr;
	switch (surface)
	{
	case DefaultFleshSurface:
		HitEffect = DefaultFleshHitEffect;
		break;
	case VurenableFleshSurface:
		HitEffect = VurenablrFleshHitEffect;
		break;
	default:
		HitEffect = DefaultHitEffect;
		break;
	}
	if (HitEffect)
	{
		FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);
		FVector ShotDirection = ImpactPoint - MuzzleLocation;
		ShotDirection.Normalize();
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitEffect, ImpactPoint, ShotDirection.Rotation());
	}

}

void ASWeapon::CoolDown()
{
	bIsCountingCooldown = false;
	if (bIsInFireCooldown == false)
	{
		bIsInFireCooldown = true;
		GetWorld()->GetTimerManager().SetTimer(fireCooldownHandler, this, &ASWeapon::CoolDown, FireCooldownDuration, false, FireCooldownDuration);
	}
	else
	{
		bIsInFireCooldown = false;
	}
}

void ASWeapon::StartFire()
{

	float timeFromPrevShot = GetWorld()->TimeSeconds - lastTimeFire;
	float firstDelay = FMath::Max(DelayBetweenShotsSeconds - timeFromPrevShot,0.0f);

	GetWorld()->GetTimerManager().SetTimer(fireTimerHandler, this, &ASWeapon::Fire, DelayBetweenShotsSeconds, true, firstDelay);
}

void ASWeapon::StopFire()
{
	GetWorld()->GetTimerManager().ClearTimer(fireTimerHandler);
	GetWorld()->GetTimerManager().ClearTimer(fireCooldownHandler);
	bIsCountingCooldown = false;
	bIsInFireCooldown = false;
}

void ASWeapon::PlayFireEffect(FVector traceTarget)
{
	if (MuzzleEffect)
	{
		UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, MeshComp, MuzzleSocketName);
	}
	FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);
	if (SmokeEffect)
	{
		
		UParticleSystemComponent* PS = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SmokeEffect, MuzzleLocation);
		PS->SetVectorParameter(TraceParameterName, traceTarget);

	}
	if (AudioFireEffect)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), AudioFireEffect, MuzzleLocation);
	}
	APawn* owner = Cast<APawn>(GetOwner());
	if (owner)
	{
		APlayerController* PC = Cast<APlayerController>(owner->GetController());
		if (PC && CameraShake)
		{
			PC->ClientPlayCameraShake(CameraShake);
		}

	}
}

void ASWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME_CONDITION(ASWeapon, HitScanTrace,COND_SkipOwner);
}
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "SWeapon.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent;
class USHealth;
UCLASS()
class COOPGAME_API ASCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void MoveForward(float value);
	void MoveRight(float value);
	void BeginCrouch();
	void EndCrouch();
	void DoJump();
	void BeginZoom();
	void EndZoom();
	

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UCameraComponent* CameraComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USpringArmComponent* SpringArmComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USHealth* HealthComp;

	float defaultFOV;
	UPROPERTY(EditDefaultsOnly, Category="Player")
	float zoomFOV = 65.0;
	UPROPERTY(EditDefaultsOnly, Category = "Player", meta=(ClampMin=0.1, ClampMax=100.0))
	float zoomVelocity = 20.0;
	bool bZoom = false;
	UPROPERTY(Replicated)
	ASWeapon* currentWeapon;
	UPROPERTY(EditDefaultsOnly, Category = "Player")
	TSubclassOf<ASWeapon> startWeaponClass;
	UPROPERTY(VisibleDefaultsOnly, Category = "Player")
	FName WeaponSocketName = "WeaponSocket";
	UPROPERTY(EditDefaultsOnly, Category = "Player")
	TSubclassOf<ASWeapon> secondaryStartWeaponClass;
	UPROPERTY(EditDefaultsOnly, Category = "Player")
	int chanceForSecondary = 0;;
	UFUNCTION()
	void OnHealthChanged(USHealth* HealthCom, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
	UPROPERTY(Replicated,BlueprintReadOnly, Category = "Player")
	bool bDied = false;
	UFUNCTION( BlueprintCallable, Category = "Player")
	bool GetDied();
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual FVector GetPawnViewLocation() const override;
	UFUNCTION(BlueprintCallable, Category = "Player")
	void StartFire();
	UFUNCTION(BlueprintCallable, Category = "Player")
	void StopFire();
	UFUNCTION(Category = "Player")
	void SetName();
	UFUNCTION(BlueprintImplementableEvent, Category = "Player")
	void SetName_BP(const FString& newName);
	FTimerHandle NameTimer;

};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "CoopGame/MenuSystem/MenuInterface.h"
#include "CoopGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class COOPGAME_API UCoopGameInstance : public UGameInstance, public IMenuInterface
{
	GENERATED_BODY()
public:
	UCoopGameInstance();
	virtual void Host() override;
	virtual void Join(FString ipAddress) override;
	UFUNCTION(BlueprintCallable)
	virtual void StartLevel() override;
	virtual void GoToMainMenu() override;
	virtual void SetPlayerName(FString newName) override;
	UFUNCTION(BlueprintCallable)
	void ShowMainMenu();
	UFUNCTION(BlueprintCallable)
	void ShowInGameMenu();
	UFUNCTION(BlueprintCallable)
	void ShowLobbyMenu();
	FString name;

private:
	TSubclassOf<class UUserWidget> MainMenuClass;
	class UMainMenuWidget* mainMenu;
	TSubclassOf<class UUserWidget> InGameMenuClass;
	class UInGameMenuWidget* inGameMenu;
	TSubclassOf<class UUserWidget> LobbyMenuClass;
	class ULobbyMenuWidget* lobbyMenu;
	FString serverIp;
};

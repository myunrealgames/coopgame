// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SGameMode.generated.h"

/**
 * 
 */
UCLASS()
class COOPGAME_API ASGameMode : public AGameModeBase
{
	GENERATED_BODY()
protected:
	
	FTimerHandle TimerHandle_BotSpawn;
	FTimerHandle TimerHandle_StartWave;
	int numberOfBotsToSpawn;
	int waveNumber = 0;
	UPROPERTY(EditDefaultsOnly, Category = "GameMode")
	float FirstPawnWaveTime = 0.0;
	UPROPERTY(EditDefaultsOnly, Category = "GameMode")
	float PawnWaveTime = 0.0;
	UPROPERTY(EditDefaultsOnly, Category = "GameMode", meta= (ClampMin = 1))
	uint8 WavesToComplete = 10;
	UPROPERTY(EditDefaultsOnly, Category = "GameMode")
	bool IncreaseBotsEachSpwan = true;
	UPROPERTY(EditDefaultsOnly, Category = "GameMode")
	uint8 SpawnIncreaseMultiply = 2;
	UPROPERTY(EditDefaultsOnly, Category = "GameMode")
	TSubclassOf<AActor> spectetingActorClass;
	UFUNCTION(BlueprintImplementableEvent, Category="GameMode")
	void SpawmNewBot();
	void StartBotsWave();
	void EndBotWave();
	void PrepareForNextWave();
	void SpawnBotTimerElappsed();
	void CheckWaveState();
	bool IsAnyPlayerAlive();
	bool gameOver = false;
	void GameOver(bool bDidWinGame);
	UFUNCTION()
	virtual void PreLogin(const FString& Options,const FString& Address,const FUniqueNetIdRepl& UniqueId,FString& ErrorMessage) override;
public:
	ASGameMode();
	virtual void StartPlay() override;
	virtual void Tick(float DeltaTime) override;
};

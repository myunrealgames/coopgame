// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SExplosiveBarrel.generated.h"

class UStaticMeshComponent;
class USHealth;
class UParticleSystem;
class URadialForceComponent;
UCLASS()
class COOPGAME_API ASExplosiveBarrel : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASExplosiveBarrel();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UStaticMeshComponent* MeshComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	URadialForceComponent* RadialForceComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USHealth* HealthComp;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Effects")
	UParticleSystem* ExplosionEffect;
	UFUNCTION()
	void OnHealthChanged(USHealth* HealthCom, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
	UPROPERTY(ReplicatedUsing=On_bExplodedRep,BlueprintReadOnly, Category = "Barrel")
	bool bExploded = false;
	UFUNCTION()
	void On_bExplodedRep();
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Explosion")
	TSubclassOf< UDamageType>  DamageTypeClass;
	UPROPERTY(EditDefaultsOnly,  Category = "Explosion")
	float BaseDamage;
	UPROPERTY(EditDefaultsOnly,  Category = "Explosion")
	float MinimumDamage;
	UPROPERTY(EditDefaultsOnly,  Category = "Explosion")
	float DamageInnerRadius;
	UPROPERTY(EditDefaultsOnly,  Category = "Explosion")
	float DamageOuterRadius;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Effects")
	USoundBase* ExplosionSoundEffect;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Effects")
	UMaterial* ExplodedMaterial;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

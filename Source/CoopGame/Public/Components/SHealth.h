// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SHealth.generated.h"
DECLARE_DYNAMIC_MULTICAST_DELEGATE_SixParams(FHealthChangedSignature, USHealth*, HealthComp, float, Health, float, HealthDelta, const class UDamageType*, DamageType, class AController*, InstigatedBy, AActor*, DamageCauser);

UCLASS( ClassGroup=(COOP), meta=(BlueprintSpawnableComponent) )
class COOPGAME_API USHealth : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USHealth();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	UPROPERTY(ReplicatedUsing=Rep_OnHealth, BlueprintReadOnly, Category = "Health")
	float Health;
	UFUNCTION()
	void Rep_OnHealth(float oldHealth);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float DefaultHealth = 100.f;
	UFUNCTION()
	void OnTakeAnyDamageHandler(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,  Category = "Health")
	uint8 TeamId = 255;
	bool bIsActiveShield = false;
public:
	UPROPERTY(BlueprintAssignable, Category ="Events")
	FHealthChangedSignature OnHealthChanged;
	UFUNCTION(BlueprintCallable)
	float GetHealth() const;
	UFUNCTION(BlueprintCallable  )
	void Heal(float healAmount);
	UFUNCTION(BlueprintCallable, Category = "Health")
	static bool IsFriendly(AActor* Actor1, AActor* Actor2);
	uint8 GetTeamId() const;
	UFUNCTION(BlueprintCallable, Category = "Health")
	void SetShield(bool activate);
	
};

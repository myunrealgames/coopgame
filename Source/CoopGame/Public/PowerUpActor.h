// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PowerUpActor.generated.h"

UCLASS()
class COOPGAME_API APowerUpActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APowerUpActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditDefaultsOnly, Category = "PowerUp")
	float tickDuration;
	UPROPERTY(EditDefaultsOnly, Category = "PowerUp")
	int numOfTicks;
	int tickPassed;
	
	bool ended;
	FTimerHandle PowerUpTickTimer;
	UFUNCTION()
	void OnPowerUpTick();
	UPROPERTY(ReplicatedUsing = OnRep_PowerUpActive)
	bool isPowerUpActive = false;
	UFUNCTION()
	void OnRep_PowerUpActive();
	UFUNCTION(BlueprintImplementableEvent)
	void PowerUpActiveStateChanged(bool IsActive);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintImplementableEvent)
	void BeginPowerUp(AActor* activator);
	UFUNCTION(BlueprintImplementableEvent)
	void PowerupTicked();
	UFUNCTION(BlueprintImplementableEvent)
	void EndPowerUp();
	void ActivatePowerUp(AActor* activator);
};

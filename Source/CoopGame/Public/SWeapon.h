// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SWeapon.generated.h"
class USkeletalMeshComponent;
class UDamageType;
class UParticleSystem;

USTRUCT()
struct FHitScanTrace
{
	GENERATED_BODY()
public:
	UPROPERTY()
	TEnumAsByte<EPhysicalSurface> PhysicalSurface;
	UPROPERTY()
	FVector_NetQuantize TraceTo;
	UPROPERTY()
	int changeForcer = 0;
};

UCLASS()
class COOPGAME_API ASWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASWeapon();

protected:
	// Called when the game starts or when spawned
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USkeletalMeshComponent* MeshComp;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapons")
	TSubclassOf<UDamageType> DamageType;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapons")
	UParticleSystem* DefaultHitEffect;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapons")
	UParticleSystem* DefaultFleshHitEffect;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapons")
	UParticleSystem* VurenablrFleshHitEffect;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapons")
	UParticleSystem* MuzzleEffect;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapons")
	USoundBase* AudioFireEffect;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Weapons")
	FName MuzzleSocketName;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapons")
	UParticleSystem* SmokeEffect;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Weapons")
	FName TraceParameterName;
	UPROPERTY(EditDefaultsOnly, Category = "Weapons")
	TSubclassOf<UCameraShake> CameraShake;
	UPROPERTY(EditDefaultsOnly, Category = "Weapons")
	float BaseDamage=20.0f;
	UPROPERTY(EditDefaultsOnly, Category = "Weapons")
	float FireRate = 600.0f;
	UPROPERTY(EditDefaultsOnly, Category = "Weapons")
	float FireDurationBeforeCoolDown = 5.0f;
	UPROPERTY(EditDefaultsOnly, Category = "Weapons")
	float FireCooldownDuration = 1.0f;
	/*Bullet spread in degrees*/
	UPROPERTY(EditDefaultsOnly, Category = "Weapons", meta = (ClampMin=0.0f))
	float BulletSpeard ;
	void PlayFireEffect(FVector traceTarget);
	virtual void Fire();

	void PlayHitEffect(EPhysicalSurface surface, FVector ImpactPoint);
	UFUNCTION()
	void CoolDown();

	FTimerHandle fireTimerHandler;
	FTimerHandle fireCooldownHandler;
	float DelayBetweenShotsSeconds = 0.0;
	virtual void BeginPlay();
	float lastTimeFire = 0;
	UFUNCTION(Reliable, Server, WithValidation)
	void ServerFire();
	UPROPERTY(ReplicatedUsing=On_HitScanTrace_Rep)
	FHitScanTrace HitScanTrace;
	UFUNCTION()
	void On_HitScanTrace_Rep();
	bool bIsInFireCooldown = false;
	bool bIsCountingCooldown = false;

public:	
	virtual void StartFire();
	virtual void StopFire();
	
	


};

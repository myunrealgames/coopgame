// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "STrackerPawn.generated.h"
class USHealth;
class USphereComponent;
class UAudioComponent;
UCLASS()
class COOPGAME_API ASTrackerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASTrackerPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UStaticMeshComponent* meshComp;
	UPROPERTY(EditDefaultsOnly, Category = "Components")
	USphereComponent* sphereComp;
	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UAudioComponent* audioComp;
	void GetNextPathPoint();
	FVector nextPoint;
	UPROPERTY(EditDefaultsOnly, Category = "Tracker")
	float minDistanceToNextPoint;
	UPROPERTY(EditDefaultsOnly, Category = "Tracker")
	float forcePower;
	UPROPERTY(EditDefaultsOnly, Category = "Tracker")
	float damage;
	UPROPERTY(EditDefaultsOnly, Category = "Tracker")
	float explosionRadius;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USHealth* HealthComp;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Effects")
		UParticleSystem* ExplosionEffect;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Effects")
		USoundBase* ExplosionSoundEffect;
	UFUNCTION()
	void OnHealthChanged(USHealth* HealthCom, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
	UMaterialInstanceDynamic* MatInst;
	void Explode();
	bool exploded = false;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
private:
	ACharacter* GetClosestCharacter();
	
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickUpActor.generated.h"
class USphereComponent;
class APowerUpActor;
UCLASS()
class COOPGAME_API APickUpActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickUpActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(VisibleAnywhere, Category = "Components")
	USphereComponent* sphereComp;
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UDecalComponent* decalComp;
	APowerUpActor* currentPowerUp;
	UPROPERTY(EditInstanceOnly, Category = "PowerUpClass")
	TSubclassOf< APowerUpActor> powerupClass;
	UPROPERTY(EditDefaultsOnly, Category = "PowerUpClass")
	float respawnRate;
	UFUNCTION()
	void Respawm();
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
};
